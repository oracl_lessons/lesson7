-----jadvaldagi sonli malumotlarni 10 ga qushish,ayirish,kupaytirish,bulish va jadvalga chiqarish--

   create table mashina_tbl(pno numeric,cno number,nom varchar2(10),narx binary_float,tur char)
   select *from mashina_tbl
   insert into mashina_tbl(pno,cno,nom,narx,tur)values(
       SQ_mashina_tbl_TB.Nextval,1,'AAAAA',12.34,'A')
   select nom,narx+10,narx-10,narx*10,narx/10 from mashina_tbl

   select nom, narx + 10 as PLUS , narx-10 as MNUS, narx*10 as MULTI, narx/10 as DIV from mashina_tbl;


   ----------------------jadvaladagi string(matnli) malumotlarga suz qushish----------------------------

   create table mashina1_btl(id int not null primary key,nom varchar2(10))
   select *from mashina1_btl
   insert into mashina1_btl(id,nom)values(
       SQ_mashina1_btl_TBL.Nextval,'Fit')

    select id, 'the ' || nom from mashina1_btl



    select id, nom || '''s price' from mashina1_btl


    ------------------jadvaldagi vaqtlarga vaqt ('+','-','*','/') amallari-------------------------------

    create table ish_tbl(id int not null primary key,ism varchar2(20),soat date)
    select *from ish_tbl
    insert into ish_tbl(id,ism,soat)values(
       SQ_ish_tbl.Nextval,'Fatima',to_date('11:30:00','hh-mi-ss'))
    select id,ism,soat-(1/24) from ish_tbl   

    ------------------------------------------------------------------------------------------------------

create table ishchi_table(id int not null primary key,
       ism varchar2(20),
       yosh number,
       oylik numeric)
       
select *from ishchi_table

insert into ishchi_table(id,ism,yosh,oylik)values(
       SQ_ishchi_tbl.Nextval,
       'Fatima',35,980000)
       
select *from ishchi_table where oylik >= yosh*25000
    ----------------------------------------------------------------------------------------------------

    create table oxrana_tbl(id int not null primary key,name varchar2(20),oylik_maoshi int)

    select *from oxrana_tbl
       insert into oxrana_tbl(id,name,oylik_maoshi)values(
              1,'Ezoz',500000)
       select name, oylik_maoshi+10,oylik_maoshi-10,oylik_maoshi*10,oylik_maoshi/10 from oxrana_tbl;
       
       select 'the' || name from oxrana_tbl;
       select id, name || '''jon' from oxrana_tbl;
       
       create view "oxrana ismi va oylik maoshi" as select oxrana_tbl.name,oxrana_tbl.oylik_maoshi from oxrana_tbl;
       select *from "oxrana ismi va oylik maoshi"