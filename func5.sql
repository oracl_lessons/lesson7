select *from dasturchilar
create table dasturchilar(id int,
       first_name char(20),
       last_name varchar2(20),
       tugilgan_sanasi date,
       pasport_seriyasi varchar2(10),
       yashash_manzili varchar2(20),
       tel_raqami varchar2(15),
       darajasi varchar2(10),
       ish_vaqti date,
       oylik_maoshi binary_float)
       
insert into dasturchilar(
       id,
       first_name,
       last_name,
       tugilgan_sanasi,
       pasport_seriyasi,
       yashash_manzili,
       tel_raqami,
       darajasi,
       ish_vaqti,
       oylik_maoshi)values(
       2,
       'Yulchi',
       'Qurbonov',
       to_date('1996.01.02','YYYY-MM-DD'),
       'AC3426437',
       'Namangan',
       '+998932668816',
       'middle',
       to_date('08:30:00','hh-mi-ss'),
       10.500000)
       
select to_char(ish_vaqti, 'hh') as soat,to_char(ish_vaqti, 'mi') as minut from dasturchilar
select max(oylik_maoshi) as eng_kop_oylik,min (oylik_maoshi) as eng_kam_oylik from dasturchilar
select trunc(oylik_maoshi) from dasturchilar
select count(darajasi) as darajasi from dasturchilar group by id
select first_name as ismi,pasport_seriyasi from dasturchilar
select count(tugilgan_sanasi) from dasturchilar
select ceil(oylik_maoshi)as oylik_balandroq,floor(oylik_maoshi) as oyligi_pastroq from dasturchilar
select avg(oylik_maoshi) as urtacha_oylik from dasturchilar
select 'pragrammist ' || last_name programmist from dasturchilar
select to_char(tugilgan_sanasi, 'YYYY') as yil,to_char(tugilgan_sanasi, 'MM') as oy,to_char(tugilgan_sanasi, 'DD') as kun from dasturchilar
select round(oylik_maoshi) as Butun_hisobda, trunc(oylik_maoshi, 2) as TRUNC_qushimcha_hiosbda from dasturchilar
select *from dasturchilar where oylik_maoshi >= (id*10)
select sum(oylik_maoshi) from dasturchilar
select id, count(*) as darajadagilar from dasturchilar group by id


alter table dasturchilar rename column ish_vaqti to levels
alter table dasturchilar drop column levels
alter table dasturchilar rename column darajasi to levels
alter table dasturchilar rename column tugilgan_sanasi to date_of_birth
alter table dasturchilar rename column pasport_seriyasi to passport_series
alter table dasturchilar rename column yashash_manzili to residential_address
alter table dasturchilar rename column tel_raqami to phone_number
alter table dasturchilar rename column oylik_maoshi to monthly_salary


create view "dasturchilar" as select dasturchilar.id,first_name,last_name,TEL_RAQAMI,PASPORT_SERIYASI,OYLIK_MAOSHI,DARAJASI,TUGILGAN_SANASI from dasturchilar
select *from "dasturchilar"


select *from dasturchilar1
create table dasturchilar1(id int not null primary key,
       first_name varchar2(10),
       last_name char(10),
       pasport_seriyasi varchar2(100),
       tel_raqami char(15),
       yashash_manzili varchar2(20),
       darajasi varchar2(10),
       oylik_maoshi binary_float)
insert into dasturchilar1(id,
       first_name,
       last_name,
       pasport_seriyasi,
       tel_raqami,
       yashash_manzili,
       darajasi,
       oylik_maoshi)values(
       SQ_dasturchilar1.Nextval,'Umid','Akbarov','AB1453710','+998905657656','Qashqadaryo',null,4.500000)
       
       select count(darajasi) as soni, darajasi from dasturchilar1 group by darajasi
       select count(yashash_manzili) as soni,yashash_manzili from dasturchilar1 group by yashash_manzili
       select round(oylik_maoshi) as butunHisobda, trunc(oylik_maoshi, 1) floatHisobda from dasturchilar1
       select ceil(oylik_maoshi) as oyligiButunKattaHisobda,floor(oylik_maoshi) as kichikHisobda from dasturchilar1
       select first_name,last_name, NVL(darajasi, 'yo''q') from dasturchilar1
       select count(first_name) as soni,first_name from dasturchilar1 group by first_name
       select 'pragrammist ' || first_name as pragrammistlar from dasturchilar1
       select count(oylik_maoshi) as birXilOyliklar,oylik_maoshi from dasturchilar1 group by oylik_maoshi having oylik_maoshi > 10.000000
       select avg(oylik_maoshi) as urtacha_oyik,sum(oylik_maoshi) as oyliklar_yigindisi from dasturchilar1
       select max(oylik_maoshi) as engBalandOylik,min(oylik_maoshi) as engKamOylik  from dasturchilar1
       