create table func(raqam int,raqam1 binary_float,vaqt date,nom varchar2(50))
insert into func(raqam,raqam1,vaqt,nom)values(
       1,69.3,to_date('2000.12.09','YYYY-MM-DD'),'use')
select *from func
select ceil(raqam1) from func ------------CEIL,yaxlit sondan bitta balandini oladi. FLOOR-kichikini oladi
select round (raqam1) from func----------ROUND,oddiy yaxlitlash yani 5 dan pasti kichik 5 dan balandi katta qilib oladi
select trunc (raqam1, 1) from func ------ TRUNC,qirqish yani hich narsa quymasa sonni uzi,1 bulsa verguldan keyin bitta son qushib oladi
select to_char(vaqt, 'DD') AS Kun, to_char(vaqt, 'MM') AS Oy, to_char(vaqt, 'YYYY') AS Yil from func-----yil,oy,kun larni alohida qiladi
select round (vaqt, 'MM') as oy,trunc (vaqt,'DD') kun from func
select to_char(vaqt, 'YYYY') as yil,to_char(vaqt, 'MM') as oy,to_char(vaqt, 'DD') as kun from func
select raqam1, vaqt, nvl(nom, 'yo''q') as nom from func------ qiymat kiritilmagan joyga yoq suzni yozib beradi
select count(vaqt) from func----- COUNT,column nomi kiritilsa usha columnda nechta malumot borligini sanaydi
select sum(raqam) from func------SUM,column nomi kiritilsa yigindisini chiqarib beradi
select avg(raqam1) from func------AVG,column nomi kiritilsa usha columndagi raqamlarning urta arifmetigini chiqarib beradi
select min(vaqt) from func ----MIN,column nomi kiritilsa usha columndagi eng kichigini chiqarib beradi
select max(vaqt) from func-----MAX,column nomi kiritilsa usha columndagi eng kattasini chiqarib beradi
select nom, sum(raqam) as sum_yigindi from func group by nom-----ORDER BY guruppalash---having groupBy ning sharti kichik yoki katta bulsin
