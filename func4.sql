select *from buxgalter
create table buxgalter(
       id int,
       first_name varchar2(20),
       last_name character(20),
       tugilgan_sanasi date,
       yashash_manzili varchar2(20),
       oylik_maoshi binary_float,
       ishga_kelish_vaqti date)
insert into buxgalter(id,
       first_name,
       last_name,
       tugilgan_sanasi,
       yashash_manzili,
       oylik_maoshi,
       ishga_kelish_vaqti)values(
       3,
       'Sanjar',
       'Nazarov',
       to_date('1994.11.27','YYYY-MM-DD'),
       'Sirdaryo',
       1.550000,
       to_date('08:30:00','hh-mi;ss'))
select to_char(ishga_kelish_vaqti, 'hh') as soati ,to_char(ishga_kelish_vaqti, 'mi') as minut from buxgalter
select count(*) as nechchi_kishi_zem from buxgalter group by yashash_manzili
select sum(oylik_maoshi) from buxgalter
select trunc(oylik_maoshi,2) from buxgalter
select max(oylik_maoshi) as eng_katta_oylik, min(oylik_maoshi) as eng_kam_oylik from buxgalter
select 'janob ' || first_name as "janoblar"  from buxgalter
select 'janob ' || last_name as "janoblarni_familiyasi" from buxgalter
select first_name, oylik_maoshi*10 as "oyligi" from buxgalter
select avg(oylik_maoshi) from buxgalter
select to_char(tugilgan_sanasi, 'YYYY') as yil, to_char(tugilgan_sanasi, 'MM') as oy, to_char(tugilgan_sanasi, 'DD') as kuni from buxgalter 
select min(tugilgan_sanasi) as yoshi_kattasi,max(tugilgan_sanasi) as yoshi_kichigi  from buxgalter
