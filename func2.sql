create table kompyuter(id int not null primary key,name varchar2(10),model varchar2(10),narxi number)
select *from kompyuter
insert into kompyuter(id,name,model,price)values(
       SQ_kompyuter.Nextval,'macbook','ch2',10000000);
select id, 'komp' || name from kompyuter

alter table kompyuter rename column narxi to price
       
select id,name,price*10 from kompyuter;
create view "kompyuter nomi va price" as select kompyuter.name,kompyuter.price from kompyuter;
select *from "kompyuter nomi va price"



-------------------------------------------------------------------------------------------------------------------------

create table shifokorlar(id int not null primary key,name varchar2(20),ishga_kelish_vaqti date)
insert into shifokorlar(id,name,ishga_kelish_vaqti)values(
       SQ_shifokorlar.Nextval,'  murodaliyev akbar',to_date('08:30:00','hh:mi:ss'))
select name, ishga_kelish_vaqti+(1/24) from shifokorlar;
select *from shifokorlar;
select name,
       trim (name) N,
       initcap(name),
       substr(trim(name),1,instr(trim(name),' ','1')-1) familya,
       substr(trim(name),instr(trim(name),' ','1')+1) ism
from shifokorlar;
-------------------------------------------------------------------------------------------------------

create table texts(alif char(20),mobif varchar2(20))
insert into texts(alif,mobif,ish_vaqti)values(
       'andrey','george',to_date('09:30:00','hh:mi:ss'))
alter table texts add (ish_vaqti date)
select *from texts
select 'the' || alif,mobif from texts
select mobif || '''s' from texts

create view "alif va mobifning ish vaqti" as select texts.alif,texts.ish_vaqti from texts
select *from "alif va mobifning ish vaqti"